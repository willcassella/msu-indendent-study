// Transform.cpp - Copyright 2013-2015 Will Cassella, All Rights Reserved

#include <WillowMath/Mat4.h>
#include "Transform.h"

///////////////////
///   Methods   ///

void Transform::Translate(Vec3 offset, bool isLocal)
{
    if (isLocal)
    {
        offset = Mat4::Rotate(Rotation) * offset;
    }

    Position += offset;
}

void Transform::Rotate(const Vec3& axis, float angle, bool isLocal)
{
    Rotation.RotateByAxisAngle(axis, angle, isLocal);
}
