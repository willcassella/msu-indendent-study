// Terrain.cpp - Copyright 2013-2015 Will Cassella, All Rights Reserved

#include <QImage>
#include "Terrain.h"

////////////////////////
///   Constructors   ///

Terrain::Terrain()
{
    _heightMap = new QImage();
}

Terrain::Terrain(const Terrain& copy)
{
    _heightMap = new QImage(*copy._heightMap);
    _heightMapPath = copy._heightMapPath;
}

Terrain::Terrain(Terrain&& move)
{
    _heightMap = move._heightMap;
    _heightMapPath = move._heightMapPath;
    move._heightMap = nullptr;
    move._heightMapPath = "";
}

Terrain::~Terrain()
{
    delete _heightMap;
}

///////////////////
///   Methods   ///

float Terrain::GetHeightAtPoint(const Vec2& worldPoint) const
{
    Vec2 terrainPoint = WorldPointToTerrainPoint(worldPoint);

    auto corners = GetPatchHeight(terrainPoint);
    float patchSize = GetPatchSize();
    Vec2 patchPoint = TerrainPointToPatchPoint(terrainPoint);

    // Interpolate between the corners of the patch
    float lowerInterp = (patchSize - patchPoint.X)/(patchSize)*std::get<0>(corners) + (patchPoint.X)/(patchSize)*std::get<1>(corners);
    float upperInterp = (patchSize - patchPoint.X)/(patchSize)*std::get<2>(corners) + (patchPoint.X)/(patchSize)*std::get<3>(corners);
    float height = (patchSize - patchPoint.Y)/(patchSize)*lowerInterp + (patchPoint.Y)/(patchSize)*upperInterp;

    return height * Scale.Y;
}

Vec3 Terrain::GetNormalAtPoint(const Vec2& worldPoint) const
{
    float patchSize = GetPatchSize();
    Vec2 terrainPoint = WorldPointToTerrainPoint(worldPoint);
    auto corners = GetPatchHeight(terrainPoint);

    Vec3 bottomToTop = Vec3(0, std::get<2>(corners) - std::get<0>(corners), patchSize) * Scale;
    Vec3 leftToRight = Vec3(patchSize, std::get<1>(corners) - std::get<0>(corners), 0) * Scale;

    return Vec3::Cross(bottomToTop, leftToRight).Normalize();
}

const string& Terrain::GetHeightMapPath() const
{
    return _heightMapPath;
}

void Terrain::SetHeightMapPath(const string& path)
{
    if (path != _heightMapPath)
    {
        _heightMap->load(path.c_str());
        _heightMapPath = path;
    }
}

void Terrain::ForceReloadHeightMap()
{
    _heightMap->load(_heightMapPath.c_str());
}

float Terrain::GetPatchSize() const
{
    return 1.f / NumPatches;
}

Vec2 Terrain::WorldPointToTerrainPoint(const Vec2& worldPoint) const
{
    Vec2 terrainPoint = worldPoint;

    // Bring the point into object space
    terrainPoint.X /= Scale.X * 2;
    terrainPoint.Y /= Scale.Z * 2;

    // Bring the point into terrain space
    terrainPoint.X += 0.5f;
    terrainPoint.Y += 0.5f;

    return terrainPoint;
}

Vec2 Terrain::TerrainPointToPatchPoint(const Vec2& terrainPoint) const
{
    float patchSize = GetPatchSize();
    float x = fmod(terrainPoint.X, patchSize); // X position across the patch (l-r)
    float y = fmod(terrainPoint.Y, patchSize); // Y position going up the patch (b-t)

    return Vec2(x, y);
}

Vec2 Terrain::TerrainPointToBitmapPoint(const Vec2& terrainPoint) const
{
    float x = terrainPoint.X * _heightMap->width();
    float y = (1 - terrainPoint.Y) * _heightMap->height();
    return Vec2(x, y);
}

pair<Vec2, Vec2> Terrain::GetPatchCorners(const Vec2& terrainPoint) const
{
    float patchSize = GetPatchSize();
    Vec2 patchPoint = TerrainPointToPatchPoint(terrainPoint);

    Vec2 lowerLeft = terrainPoint - patchPoint;
    Vec2 upperRight = Vec2(lowerLeft.X + patchSize, lowerLeft.Y + patchSize);

    return pair<Vec2, Vec2>(lowerLeft, upperRight);
}

tuple<float, float, float, float> Terrain::GetPatchHeight(const Vec2& terrainPoint) const
{
    pair<Vec2, Vec2> patchCorners = GetPatchCorners(terrainPoint);

    // Get height at corners of patch
    float lowerLeftHeight = SampleHeightmapAtPoint(patchCorners.first);
    float lowerRightHeight = SampleHeightmapAtPoint(Vec2(patchCorners.second.X, patchCorners.first.Y));
    float upperLeftHeight = SampleHeightmapAtPoint(Vec2(patchCorners.first.X, patchCorners.second.Y));
    float upperRightHeight = SampleHeightmapAtPoint(Vec2(patchCorners.second));

    return std::make_tuple(lowerLeftHeight, lowerRightHeight, upperLeftHeight, upperRightHeight);
}

float Terrain::SampleHeightmapAtPoint(const Vec2& terrainPoint) const
{
    // Convert the terrain point into image coordinates
    Vec2 bitmapPoint = TerrainPointToBitmapPoint(terrainPoint);

    // Make sure we aren't outside the image
    int x = clamp((int)bitmapPoint.X, 0, _heightMap->width() - 1);
    int y = clamp((int)bitmapPoint.Y, 0, _heightMap->height() - 1);

    // Sample the point and return
    return (float)qRed(_heightMap->pixel(x, y)) / 255;
}

/////////////////////
///   Operators   ///

Terrain& Terrain::operator=(const Terrain& copy)
{
    if (&copy != this)
    {
        delete _heightMap;
        _heightMap = new QImage(*copy._heightMap);
        _heightMapPath = copy._heightMapPath;
    }

    return This;
}

Terrain& Terrain::operator=(Terrain&& move)
{
    if (&move != this)
    {
        delete _heightMap;
        _heightMap = move._heightMap;
        _heightMapPath = move._heightMapPath;

        move._heightMap = nullptr;
        move._heightMapPath = "";
    }

    return This;
}
