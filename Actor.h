// Actor.h
#pragma once

#include "GameObject.h"

/** Actors are GameObjects that automatically subscribe to the scene's "Tick" event
* So they can run logic every frame. You do not need to extend Actor to do this,
* but Actors do it automatically. */
class Actor : public GameObject
{
    ////////////////////////
    ///   Constructors   ///
public:

    Actor(Scene& scene);

    /////////////////////////
    ///   EventHandlers   ///
public:

    /** Handler for the "Tick" event */
    virtual void Tick(float timeDelta);
};
