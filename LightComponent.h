// LightComponent.h
#pragma once

#include <WillowMath/Vec3.h>
#include "Component.h"

class LightComponent : public Component
{
    ///////////////////////
    ///   Inner Types   ///
public:

    enum class Type : uint32
    {
        AMBIENT, DIRECTIONAL, SPOT
    };

    ////////////////////////
    ///   Constructors   ///
public:

    LightComponent(GameObject& owner);

    //////////////////
    ///   Fields   ///
public:

    /** The color of the light */
    Vec3 Color = Vec3(1, 1, 1);

    /** The intensity of the light */
    float Intensity = 1;

    /** The type of this light */
    Type type = Type::SPOT;
};
