// Component.cpp - Copyright 2013-2015 Will Cassella, All Rights Reserved

#include <algorithm>
#include "Component.h"
#include "GameObject.h"
#include "Scene.h"

////////////////////////
///   Constructors   ///

Component::Component(GameObject& owner)
    : _owner(&owner)
{
    // Add this Component to the owner's collection of Components
    owner._components.push_back(this);
}

Component::~Component()
{
    // Remove this Component from the owner's collection of Components (This is why we can't have nice things)
    _owner->_components.erase(std::remove(_owner->_components.begin(), _owner->_components.end(), this), _owner->_components.end());
}

///////////////////
///   Methods   ///

GameObject& Component::GetOwner()
{
    return *_owner;
}

const GameObject& Component::GetOwner() const
{
    return *_owner;
}

bool Component::IsEnabled() const
{
    return _isEnabled;
}

void Component::Enable()
{
    if (!_isEnabled || !_isInitialized)
    {
        _owner->GetScene()._components[typeid(This)].push_back(this);
        _isEnabled = true;
        _isInitialized = true;
        OnEnable();
    }
}

void Component::OnEnable()
{
    // Do nothing (default)
}

void Component::Disable()
{
    if (!_isInitialized)
    {
        _isEnabled = false;
        _isInitialized = true;
    }
    else if (_isEnabled)
    {
        auto& vec = _owner->GetScene()._components[typeid(This)];
        vec.erase(std::remove(vec.begin(), vec.end(), this), vec.end());
        _isEnabled = false;
        OnDisable();
    }
}

void Component::OnDisable()
{
    // Do nothing (default)
}

/////////////////////
///   Operators   ///

Component& Component::operator=(const Component& /* copy */)
{
    // Do nothing (prevents copying of "_owner")
    return This;
}

Component& Component::operator=(Component&& /* move */)
{
    // Do nothing (prevents copying of "_owner")
    return This;
}
