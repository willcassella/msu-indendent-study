// CameraComponent.h - Copyright 2013-2015 Will Cassella, All Rights Reserved
#pragma once

#include <WillowMath/Mat4.h>
#include "Component.h"

class CameraComponent final : public Component
{
    ////////////////////////
    ///   Constructors   ///
public:

    CameraComponent(GameObject& owner);

    //////////////////
    ///   Fields   ///
public:

    /** Horizontal field of view (in degrees) */
    float HFOV = 90;

    /** Vertical field of view (in degrees) */
    float VFOV = 60;

    /** Distance to near clipping plane */
    float ZNear = 0.1f;

    /** Distance to far clipping plane */
    float ZFar = 100;

    ///////////////////
    ///   Methods   ///
public:

    /** Generates a perspective projection matrix for this Camera */
    Mat4 GetProjectionMatrix() const;
};
