// Pagoda.cpp

#include "Pagoda.h"

////////////////////////
///   Constructors   ///

Pagoda::Pagoda(Scene &scene)
    : Prop(scene), Lantern(This)
{
    transform.Scale /= 4.5;
    Mesh.Mesh = "assets/pagoda/pagoda.wmesh";
    Mesh.Material = "assets/pagoda/pagoda.wmat";
}
