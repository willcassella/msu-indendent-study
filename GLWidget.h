// GLWidget.h
#pragma once

#include <string>
#include <vector>
#include <unordered_map>
#include <QOpenGLWidget>
#include <QOpenGLFunctions_3_3_Core>
#include <QTimer>
#include <WillowMath/Mat4.h>
#include <WillowMath/Vec2.h>
using std::unordered_map;
using std::pair;
using std::string;
using std::vector;

////////////////////////////////
///   Forward-declarations   ///

/** Defined in "Scene.h" */
struct Scene;

/////////////////
///   Types   ///

class GLWidget : public QOpenGLWidget, protected QOpenGLFunctions_3_3_Core
{
    Q_OBJECT

    ///////////////////////
    ///   Inner Types   ///
private:

    struct Mesh final
    {
        GLuint VAO = 0;
        GLuint VertexBuffer = 0;
        GLuint ElementBuffer = 0;
        uint32 NumElements = 0;
    };

    struct Material final
    {
        GLuint ID = 0;
        vector<pair<GLint, GLuint>> Textures;
    };

    ////////////////////////
    ///   Constructors   ///
public:

    GLWidget(QWidget* parent = nullptr);
    GLWidget(const GLWidget& copy) = delete;
    GLWidget(GLWidget&& move) = delete;
    ~GLWidget() override;

    /////////////////
    ///   Slots   ///
public slots:

    /** Called every 16 milliseconds, updates and renders the scene */
    void Tick();

    ///////////////////
    ///   Methods   ///
protected:

    void initializeGL() override;
    void resizeGL(int width, int height) override;
    void paintGL() override;

    void mousePressEvent(QMouseEvent* event) override;
    void mouseMoveEvent(QMouseEvent* event) override;
    void keyPressEvent(QKeyEvent* event) override;
    void keyReleaseEvent(QKeyEvent* event) override;

private:

    /** Loads the image at the given path into the given texture buffer */
    void loadTexture(GLuint buffer, const string& path);

    /** Retreives the texture at the given path from the texture table,
    * loading a new one if not found */
    GLuint getTexture(const string& path);

    /** Cleans up all textures */
    void clearTextures();

    /** Loads the shader at the given path and returns its buffer */
    GLuint loadShader(const string& path, GLenum type);

    /** Retreives the shader at the given path from the shader table,
    * loading a new one if not found */
    GLuint getShader(const string& path, GLenum type);

    /** Cleans up all shaders */
    void clearShaders();

    /** Loads the mesh at the given path at returns its buffer */
    Mesh loadMesh(const string& path);

    /** Retrieves the mesh at the given path from the mesh table,
    * loading a new one if not found */
    const Mesh& getMesh(const string& path);

    /** Cleans up all meshes */
    void clearMeshes();

    /** Loads the material at the given path and returns its buffer */
    Material loadMaterial(const string& path);

    /** Retrieves the material at the given path from the material table,
    * loading a new one if not found */
    const Material& getMaterial(const string& path);

    /** Cleans up all materials */
    void clearMaterials();

    /** Initializes the OpenGL buffer for holding terrain */
    void initializeTerrain();

    /** Generates the terrain mesh using the currently set resolution */
    void generateTerrain();

    /** Renders the currently loaded terrain with the given view and projection matrices */
    void renderTerrain(const Mat4& view, const Mat4& projection);

    /////////////////////
    ///   Operators   ///
public:

    GLWidget& operator=(const GLWidget& copy) = delete;
    GLWidget& operator=(GLWidget&& move) = delete;

    ////////////////
    ///   Data   ///
private:

    /** Scene data */
    Scene* _scene = nullptr;

    /** Deferred rendering buffers */
    GLuint _gBuffer = 0;
    GLuint _depthBuffer = 0;
    GLuint _positionBuffer = 0;
    GLuint _normalBuffer = 0;
    GLuint _diffuseBuffer = 0;
    GLuint _specularBuffer = 0;
    GLuint _screenVAO = 0;
    GLuint _screenVBO = 0;
    GLuint _screenProgram = 0;

    /** StaticMeshComponent data */
    unordered_map<string, Mesh> _meshes;
    unordered_map<string, Material> _materials;
    unordered_map<string, GLuint> _textures;
    unordered_map<string, GLuint> _shaders;
    GLuint _instanceBuffer = 0;

    /** Terrain data */
    GLuint _terrainVAO = 0;
    GLuint _terrainBuffer = 0;
    GLuint _terrainProgram = 0;
    GLuint _terrainHeightMap = 0;
    uint32 _terrainNumPatches = 1;
    string _terrainHeightMapPath;

    /** 'Tick()' timer */
    QTimer _timer;

    /** Input stuff */
    bool _forward = false;
    bool _backward = false;
    bool _left = false;
    bool _right = false;
    bool _shift = false;
    Vec2 _lastPoint;
};
