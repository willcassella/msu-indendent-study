// Levels.cpp

#include "Levels.h"
#include "Vegetation.h"
#include "FPSCamera.h"
#include "Firefly.h"
#include "Skybox.h"
#include "Pagoda.h"

/////////////////////
///   Functions   ///

Scene* LoadForest()
{
    Scene* scene = new Scene();

    // Set up terrain
    scene->MapTerrain.SetHeightMapPath("levels/forest/heightmap.png");
    scene->MapTerrain.Texture1 = "images/grass.jpg";
    scene->MapTerrain.Texture2 = "images/riverbed.jpg";
    scene->MapTerrain.Specular = "images/black.png";
    scene->MapTerrain.TextureMask = "levels/forest/textureMask.png";
    scene->MapTerrain.Texture1Size = 30;
    scene->MapTerrain.Texture2Size = 10;
    scene->MapTerrain.Scale = Vec3(25, 10, 25);
    scene->MapTerrain.NumPatches = 250;

    // Spawn a skybox
    auto skybox = scene->Spawn<Skybox>().lock();
    skybox->Sun.Color = Vec3(0.343f, 0.458f, 0.905f);
    skybox->Sun.Intensity = 0.05f;

    // Spawn a camera
    scene->Spawn<FPSCamera>();

    // Spawn a pagoda
    auto pagoda = scene->Spawn<Pagoda>().lock();
    pagoda->transform.Position = Vec3(-5, 0, 10);
    pagoda->SnapToTerrain();

    // Spawn some ferns
    for (auto& fern : scene->SpawnDistribution<Vegetation>("levels/forest/fernDist.png", Vec2(1, 1), true))
    {
        auto sFern = fern.lock();
        sFern->Mesh.Mesh = "assets/fern/fern.wmesh";
        sFern->Mesh.Material = "assets/fern/fern.wmat";
    }

    // Spawn some rocks
    for (auto& rock : scene->SpawnDistribution<Vegetation>("levels/forest/rockDist.png", Vec2(0.75f, 0.75f), true))
    {
        auto sRock = rock.lock();
        sRock->Mesh.Mesh = "assets/rock/rock.wmesh";
        sRock->Mesh.Material = "assets/rock/rock.wmat";
        sRock->transform.Scale *= 0.2f;
    }

    // Spawn some fireflys
    for (auto& firefly : scene->SpawnDistribution<Firefly>("images/white.png", Vec2(5, 5)))
    {
        firefly.lock()->transform.Position.Y += 1;
    }

    return scene;
}
