// Event.cpp - Copyright 2013-2015 Will Cassella, All Rights Reserved

#include "Event.h"

/////////////////
///   Event   ///

////////////////////////
///   Constructors   ///

Event::Event(const string& name, std::type_index argType)
    : _name(name), _argType(argType)
{
    // All done
}

///////////////////
///   Methods   ///

const string& Event::GetName() const
{
    return _name;
}

type_index Event::GetArgType() const
{
    return _argType;
}

////////////////////////
///   TEvent<void>   ///

////////////////////////
///   Constructors   ///

TEvent<void>::TEvent(const string& name)
    : Event(name, typeid(void))
{
    // All done
}
