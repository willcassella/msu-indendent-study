// GameObject.h - Copyright 2013-2015 Will Cassella, All Rights Reserved
#pragma once

#include <vector>
#include <string>
#include <memory>
#include <WillowMath/Mat4.h>
#include "Transform.h"
using std::vector;
using std::string;
using std::weak_ptr;

////////////////////////////////
///   Forward-declarations   ///

/** Defined in "Scene.h" */
struct Scene;

/** Defined in "Component.h" */
class Component;

/////////////////
///   Types   ///

class GameObject
{
    ///////////////////////
    ///   Information   ///
public:

    friend Component;

    ////////////////////////
    ///   Constructors   ///
public:

    GameObject(Scene& scene);
    GameObject(const GameObject& copy) = delete;
    GameObject(GameObject&& move) = delete;
    virtual ~GameObject() = 0;

    //////////////////
    ///   Fields   ///
public:

    /** The name of this GameObject */
    string Name;

    /** The spacial transform of this GameObject */
    Transform transform;

    /** The GameObject to which this GameObject is attached */
    weak_ptr<GameObject> Parent;

    ///////////////////
    ///   Methods   ///
public:

    /** Returns a reference to the Scene that this GameObject belongs to */
    Scene& GetScene();

    /** Returns an immutable reference to the Scene that this GameObject belongs to */
    const Scene& GetScene() const;

    /** Returns the Model Matrix of this GameObject */
    Mat4 GetModelMatrix() const;

    /** Snaps this object to the terrain */
    void SnapToTerrain();

    /** Aligns the up vector of this object to the normal of the point on the terrain this Object occupies */
    void AlignToTerrain();

    /** Returns a collection of all the Components attached to this GameObject
    * 'includeDisabled' - Whether to include disabled components */
    vector<Component*> GetComponents(bool includeDisabled = false);

    /** Returns a collection of all the Components attached to this GameObject
    * 'includeDisabled' - Whether to include disabled components */
    vector<const Component*> GetComponents(bool includeDisabled = false) const;

    /** Returns a collection of all the Components of the given type attached to this GameObject
    * 'includeDisabled' - Whether to include disabled components */
    template <class ComponentType>
    vector<ComponentType*> GetComponentsOfType(bool includeDisabled = false)
    {
        static_assert(std::is_base_of<Component, ComponentType>::value, "The type given to \"GameObject::GetComponentsOfType<T>()\" does not extend \"Component\"");

        vector<ComponentType*> result;

        for (auto component : _components)
        {
            auto pComponent = dynamic_cast<ComponentType*>(component);

            if (pComponent)
            {
                if (includeDisabled || pComponent.IsEnabled())
                {
                    result.push_back(pComponent);
                }
            }
        }

        return result;
    }

    /** Returns a collection of all the Components of the given type attached to this GameObject
    * 'includeDisabled' - Whether to include disabled components */
    template <class ComponentType>
    vector<const ComponentType*> GetComponentsOfType(bool includeDisabled = false) const
    {
        static_assert(std::is_base_of<Component, ComponentType>::value, "The type given to \"GameObject::GetComponentsOfType<T>()\" does not extend \"Component\"");

        vector<const ComponentType*> result;

        for (auto component : _components)
        {
            auto pComponent = dynamic_cast<const ComponentType*>(component);

            if (pComponent)
            {
                if (includeDisabled || pComponent.IsEnabled())
                {
                    result.push_back(pComponent);
                }
            }
        }

        return result;
    }

    /////////////////////
    ///   Operators   ///
public:

    GameObject& operator=(const GameObject& copy) = delete;
    GameObject& operator=(GameObject&& move) = delete;

    ////////////////
    ///   Data   ///
private:

    vector<Component*> _components; // Managed by components themselves
    Scene* _scene;
};

/** This forces "GameObject" to be abstract */
inline GameObject::~GameObject() {}
