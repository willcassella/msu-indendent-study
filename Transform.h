// Transform.h - Copyright 2013-2015 Will Cassella, All Rights Reserved
#pragma once

#include <WillowMath/Vec3.h>
#include <WillowMath/Quat.h>

struct Transform final
{
    //////////////////
    ///   Fields   ///
public:

    /** The XYZ Position of the Object */
    Vec3 Position = Vec3(0, 0, 0);

    /** The XYZ Scale of the Object */
    Vec3 Scale = Vec3(1, 1, 1);

    /** The rotation of the Object (represented as a Quaternion) */
    Quat Rotation = Quat();

    /** The Velocity of the Object */
    Vec3 Velocity = Vec3(0, 0, 0);

    ///////////////////
    ///   Methods   ///
public:

    /** Transates this transform in global or local space */
    void Translate(Vec3 offset, bool isLocal = false);

    /** Rotates this transform in global or local space */
    void Rotate(const Vec3& axis, float angle, bool isLocal = false);
};
