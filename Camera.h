// Camera.h
#pragma once

#include "GameObject.h"
#include "CameraComponent.h"

/** Bare class with a CameraComponent, for if you just want to spawn in a Camera */
class Camera : public GameObject
{
    ////////////////////////
    ///   Constructors   ///
public:

    Camera(Scene& scene);

    //////////////////////
    ///   Components   ///
public:

    CameraComponent Lens;
};
