// StaticMeshComponent.h - Copyright 2013-2015 Will Cassella, All Rights Reserved
#pragma once

#include <string>
#include "Component.h"
using std::string;

class StaticMeshComponent final : public Component
{
    ////////////////////////
    ///   Constructors   ///
public:

    StaticMeshComponent(GameObject& owner);

    //////////////////
    ///   Fields   ///
public:

    /** The path to the mesh file */
    string Mesh;

    /** The path to the material file */
    string Material;
};
