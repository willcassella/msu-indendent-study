// Levels.h
#pragma once

#include "Scene.h"

/////////////////////
///   Functions   ///

/** Loads the forest level */
Scene* LoadForest();
