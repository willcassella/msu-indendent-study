// EventManager.cpp - Copyright 2013-2015 Will Cassella, All Rights Reserved

#include "EventManager.h"

///////////////////
///   Methods   ///

void EventManager::DispatchEvent(const Event& event)
{
    // Check if we have any handlers for this event
    auto eventIndex = _handlers.find(event.GetName());

    // If we do
    if (eventIndex != _handlers.end())
    {
        // Dispatch the event to each handler
        for (const auto& handler : eventIndex->second)
        {
            handler->Handle(event);
        }
    }
}
