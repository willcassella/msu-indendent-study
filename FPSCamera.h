// FPSCamera.h - Copyright 2013-2015 Will Cassella, All Rights Reserved
#pragma once

#include <WillowMath/Vec2.h>
#include "Actor.h"
#include "CameraComponent.h"

class FPSCamera : public Actor
{
    ////////////////////////
    ///   Constructors   ///
public:

    FPSCamera(Scene& scene);

    //////////////////////////
    ///   Event Handlers   ///
public:

    void Tick(float timeDelta) override;

    /** Handles a movement input from the player */
    void Move(const Vec3& dir);

    /** Handles a look input from the player */
    void Look(const Vec2& dir);

    /** Handles a sprint/boost input from the player */
    void Boost();

    /** Handles the screen changing size */
    void Resize(const Vec2& screen);

    //////////////////////
    ///   Components   ///
public:

    CameraComponent Camera;

    ////////////////
    ///   Data   ///
private:

    float _speed = 0.01f;
};
