// GameObject.cpp - Copyright 2013-2015 Will Cassella, All Rights Reserved

#include "Scene.h"
#include "Component.h"
#include "GameObject.h"

////////////////////////
///   Constructors   ///

GameObject::GameObject(Scene& scene)
    : _scene(&scene)
{
    // All done
}

///////////////////
///   Methods   ///

Scene& GameObject::GetScene()
{
    return *_scene;
}

const Scene& GameObject::GetScene() const
{
    return *_scene;
}

Mat4 GameObject::GetModelMatrix() const
{
    Mat4 localMatrix = Mat4::Translate(transform.Position) * Mat4::Rotate(transform.Rotation) * Mat4::Scale(transform.Scale);

    // Test for validity of "Parent"
    if (auto parent = Parent.lock())
    {
        return parent->GetModelMatrix() * localMatrix;
    }
    else
    {
        return localMatrix;
    }
}

void GameObject::SnapToTerrain()
{
    transform.Position.Y = _scene->MapTerrain.GetHeightAtPoint(Vec2(transform.Position.X, transform.Position.Z));
}

void GameObject::AlignToTerrain()
{
    // Get the normal vector of where we are on the terrain
    Vec3 terrainNormal = _scene->MapTerrain.GetNormalAtPoint(Vec2(transform.Position.X, transform.Position.Z));
    Vec3 upVector = Vec3(0, 1, 0);

    // Get the axis and angle between that and the up vector
    Vec3 axis = Vec3::Cross(terrainNormal, upVector);
    float angle = acosf(Vec3::Dot(terrainNormal, upVector));

    transform.Rotation = Quat(axis, angle);
}

vector<Component*> GameObject::GetComponents(bool includeDisabled)
{
    vector<Component*> result;

    for (auto component : _components)
    {
        if (includeDisabled || component->IsEnabled())
        {
            result.push_back(component);
        }
    }

    return result;
}

vector<const Component*> GameObject::GetComponents(bool includeDisabled) const
{
    vector<const Component*> result;

    for (auto component : _components)
    {
        if (includeDisabled || component->IsEnabled())
        {
            result.push_back(component);
        }
    }

    return result;
}
