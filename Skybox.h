// Skybox.h
#pragma once

#include "GameObject.h"
#include "LightComponent.h"
#include "StaticMeshComponent.h"

class Skybox : public GameObject
{
    ////////////////////////
    ///   Constructors   ///
public:

    Skybox(Scene& scene);

    //////////////////////
    ///   Components   ///
public:

    LightComponent Sun;
    StaticMeshComponent Box;
};
