// Prop.h
#pragma once

#include "GameObject.h"
#include "StaticMeshComponent.h"

/** Bare class with a StaticMeshComponent - useful for spawning in meshes with no behavior */
class Prop : public GameObject
{
    ////////////////////////
    ///   Constructors   ///
public:

    Prop(Scene& scene);

    //////////////////////
    ///   Components   ///
public:

    StaticMeshComponent Mesh;
};
