// EventHandler.h - Copyright 2013-2015 Will Cassella, All Rights Reserved
#pragma once

#include <type_traits>
#include "Event.h"

/** Abstract interface for manipulating event handlers */
class IEventHandler
{
    ////////////////////////
    ///   Constructors   ///
public:

    virtual ~IEventHandler() = default;

    ///////////////////
    ///   Methods   ///
public:

    /** Handle a given event */
    virtual void Handle(const Event& event) const = 0;
};

/** Event handler with one argument */
template <class OwnerType, typename ArgType>
class TEventHandler final : public IEventHandler
{
    ///////////////////////
    ///   Inner Types   ///
public:

    typedef void(OwnerType::*HandlerType)(ArgType);

    ////////////////////////
    ///   Constructors   ///
public:

    TEventHandler(OwnerType* object, HandlerType handler)
        : _object(object), _handler(handler)
    {
        // All done
    }

    ///////////////////
    ///   Methods   ///
public:

    /** Handles the event */
    void Handle(const Event& event) const override
    {
        typedef typename std::decay<ArgType>::type DecayedArgType;

        // If the argument of this event is compatible with the argument for this handler
        if (event.GetArgType() == typeid(DecayedArgType))
        {
            // Run the handler
            const auto& castedEvent = static_cast<const TEvent<DecayedArgType>&>(event);
            (_object->*_handler)(castedEvent.GetValue());
        }
    }

    ////////////////
    ///   Data   ///
private:

    OwnerType* _object;
    HandlerType _handler;
};

/** Specialization for event handlers with no arguments */
template <class OwnerType>
class TEventHandler <OwnerType, void> final : public IEventHandler
{
    ///////////////////////
    ///   Inner Types   ///
public:

    typedef void(OwnerType::*HandlerType)();

    ////////////////////////
    ///   Constructors   ///
public:

    TEventHandler(OwnerType* object, HandlerType handler)
        : _object(object), _handler(handler)
    {
        // All done
    }

    ///////////////////
    ///   Methods   ///
public:

    /** Handles the event */
    void Handle(const Event& /* event */) const override
    {
        // Just call the event
        (_object->*_handler)();
    }

    ////////////////
    ///   Data   ///
private:

    OwnerType* _object;
    HandlerType _handler;
};
