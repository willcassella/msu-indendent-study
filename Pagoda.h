// Pagoda.h
#pragma once

#include "Prop.h"
#include "LightComponent.h"

class Pagoda : public Prop
{
    ////////////////////////
    ///   Constructors   ///
public:

    Pagoda(Scene& scene);

    //////////////////////
    ///   Components   ///
public:

    LightComponent Lantern;
};
