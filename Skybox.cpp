// Skybox.cpp

#include "Skybox.h"

////////////////////////
///   Constructors   ///

Skybox::Skybox(Scene &scene)
    : GameObject(scene), Sun(This), Box(This)
{
    transform.Position.Y = 25;
    transform.Scale = Vec3(100, 100, 100);
    Sun.type = LightComponent::Type::DIRECTIONAL;
    Box.Mesh = "assets/skybox/skybox.wmesh";
    Box.Material = "assets/skybox/skybox.wmat";
}
