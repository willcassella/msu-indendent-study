// Scene.cpp - Copyright 2013-2015 Will Cassella, All Rights Reserved

#include "GameObject.h"
#include "Scene.h"

///////////////////
///   Methods   ///

float Scene::GetTime() const
{
    return _time;
}

void Scene::Update(float timeDelta)
{
    // Increment time
    _time += timeDelta;

    // Dispatch the "Tick" event
    eventManager.DispatchEvent(TEvent<float>("Tick", timeDelta));

    // Dispatch events
    for (const auto& event : _events)
    {
        eventManager.DispatchEvent(*event);
    }

    // Clear the event "queue"
    _events.clear();
}

void Scene::DispatchEvent(const string& eventName)
{
    _events.push_back(std::make_unique<TEvent<void>>(eventName));
}
