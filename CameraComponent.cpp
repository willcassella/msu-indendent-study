// CameraComponent.cpp - Copyright 2013-2015 Will Cassella, All Rights Reserved

#include "CameraComponent.h"

////////////////////////
///   Constructors   ///

CameraComponent::CameraComponent(GameObject& owner)
    : Component(owner)
{
    // All done
}

///////////////////
///   Methods   ///

/** Generates a perspective projection matrix for this Camera */
Mat4 CameraComponent::GetProjectionMatrix() const
{
    return Mat4::Perspective(HFOV, VFOV, ZNear, ZFar);
}
