QT += opengl designer
CONFIG -= app_bundle
CONFIG += console c++14

INCLUDEPATH += "../include"

HEADERS += *.h
SOURCES += *.cpp
