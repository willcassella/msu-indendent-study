// Vegetation.cpp

#include "Vegetation.h"
#include "Scene.h"

///////////////////////
///  Constructors   ///

Vegetation::Vegetation(Scene &scene)
    : Prop(scene)
{
    SnapToTerrain();
    AlignToTerrain();
    scene.eventManager.Bind("TerrainChanged", this, &Vegetation::OnTerrainChanged);
}

/////////////////////////
///  Event Handlers   ///

void Vegetation::OnTerrainChanged()
{
    SnapToTerrain();
    AlignToTerrain();
}
