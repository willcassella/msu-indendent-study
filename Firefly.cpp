// Firefly.cpp

#include "Firefly.h"
#include "Scene.h"

////////////////////////
///   Constructors   ///

Firefly::Firefly(Scene &scene)
    : GameObject(scene), Mesh(This), Light(This)
{
    Mesh.Mesh = "assets/firefly/firefly.wmesh";
    Mesh.Material = "assets/firefly/firefly.wmat";

    transform.Scale = Vec3(0.1f, 0.1f, 0.1f);
    Light.Color = Vec3(1, 0.929f, 0.388f);
    Light.Intensity = 0.1f;

    scene.eventManager.Bind("Tick", this, &Firefly::Tick);
}

//////////////////////////
///   Event Handlers   ///

void Firefly::Tick()
{
    transform.Position.Y += sinf(GetScene().GetTime() * 3 + transform.Position.Z) / 500;
    transform.Position.X += cosf(GetScene().GetTime() * 2 + transform.Position.Z) / 500;
}
