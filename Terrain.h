// Terrain.h - Copyright 2013-2015 Will Cassella, All Rights Reserved
#pragma once

#include <string>
#include <tuple>
#include <WillowMath/Vec2.h>
#include <WillowMath/Mat4.h>
using std::string;
using std::pair;
using std::tuple;

////////////////////////////////
///   Forward-declarations   ///

/** Defined in "<QImage>" */
class QImage;

/////////////////
///   Types   ///

/** An object encapsulating terrain.
* NOTE: For the sake of simplicity, I'm trying to emulate the way OpenGL samples textures here
* A "Terrain coordinate" is a coordinate between 0 and 1 starting at the bottom-left corner of the terrain, moving up and right */
struct Terrain final
{
	////////////////////////
	///   Constructors   ///
public:

	Terrain();
	Terrain(const Terrain& copy);
	Terrain(Terrain&& move);
	~Terrain();

    //////////////////
    ///   Fields   ///
public:

    /** The Scale of the terrain */
    Vec3 Scale = Vec3(1, 1, 1);

    /** The number of patches this terrain is made out of */
    uint32 NumPatches = 500;

    /** Textures used to describe the surface */
    string Texture1;
    string Texture2;
    string Texture3;
    string Texture4;
    string TextureMask;
    string Specular;

    // Texture sizes
    float Texture1Size = 1;
    float Texture2Size = 1;
    float Texture3Size = 1;
    float Texture4Size = 1;

    ///////////////////
    ///   Methods   ///
public:

    /** Determines the height of the terrain at the given point
    * NOTE: If the point is outside the terrain, returns the height at the closest point on the terrain */
    float GetHeightAtPoint(const Vec2& worldPoint) const;

    /** Determines the normal of the terrain at the given point */
    Vec3 GetNormalAtPoint(const Vec2& worldPoint) const;

    /** The path to the currently used heightMap */
    const string& GetHeightMapPath() const;

    /** Sets the path to the current heightmap */
    void SetHeightMapPath(const string& path);

    /** Forces reloading of the heightmap */
    void ForceReloadHeightMap();

    /** Returns the width/height (they are square) of every patch, in terrain coordinates */
    float GetPatchSize() const;

    /** Converts a world point to a terrain point */
    Vec2 WorldPointToTerrainPoint(const Vec2& worldPoint) const;

    /** Converts a terrain point to its point within its surrounding patch */
    Vec2 TerrainPointToPatchPoint(const Vec2& terrainPoint) const;

    /** Converts a terrain point to a QImage bitmap point */
    Vec2 TerrainPointToBitmapPoint(const Vec2& terrainPoint) const;

    /** Returns the terrain coordinates of the lower-left and upper-right corners of the patch that contains the given terrain point */
    pair<Vec2, Vec2> GetPatchCorners(const Vec2& bitmapPoint) const;

    /** Returns the height of the four corners of the patch containing the given point
    * NOTE: Returns all zeros if the point is outside the heightmap
    * RETURNS: <LowerLeft, LowerRight, UpperLeft, UpperRight> */
    tuple<float, float, float, float> GetPatchHeight(const Vec2& terrainPoint) const;

private:

    /** Returns the value of the heightmap at the given TerrainPoint
    * NOTE: Returns '0' if the point is outside the heightmap */
    float SampleHeightmapAtPoint(const Vec2& terrainPoint) const;

	/////////////////////
	///   Operators   ///
public:

	Terrain& operator=(const Terrain& copy);
	Terrain& operator=(Terrain&& move);
	
    ////////////////
    ///   Data   ///
private:

    QImage* _heightMap;
    string _heightMapPath;
};
