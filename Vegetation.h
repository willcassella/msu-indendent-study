// Vegetation.h
#pragma once

#include "Prop.h"

class Vegetation : public Prop
{
    ////////////////////////
    ///   Constructors   ///
public:

    Vegetation(Scene& scene);

    ////////////////////////
    ///   EventHandlers  ///
public:

    void OnTerrainChanged();
};
