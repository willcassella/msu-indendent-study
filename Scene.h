// Scene.h - Copyright 2013-2015 Will Cassella, All Rights Reserved
#pragma once

#include <queue>
#include <memory>
#include <typeindex>
#include <type_traits>
#include <QImage>
#include "EventManager.h"
#include "Terrain.h"
using std::queue;
using std::weak_ptr;
using std::shared_ptr;
using std::type_index;

////////////////////////////////
///   Forward-declarations   ///

/** Defined in "GameObject.h" */
class GameObject;

/** Defined in "Component.h" */
class Component;

struct Scene final
{
    ///////////////////////
    ///   Information   ///
public:

    friend Component;

    ////////////////////////
    ///   Constructors   ///
public:

    Scene() = default;
    Scene(const Scene& copy) = delete;
    Scene(Scene&& move) = delete;
    ~Scene() = default;

    //////////////////
    ///   Fields   ///
public:

    /** All active GameObjects in the level */
    vector<shared_ptr<GameObject>> Objects;

    /** The terrain for the map */
    Terrain MapTerrain;

    /** The event manager for the scene */
    EventManager eventManager;

    ///////////////////
    ///   Methods   ///
public:

    /** Returns the current time of the scene */
    float GetTime() const;

    /** Update the state of the scene */
    void Update(float timeDelta);

    /** Puts an event into the event queue, which is then dispatched during the next call to "Update" */
    void DispatchEvent(const string& eventName);

    /** Puts an event into the event queue, which is then dispatched during the next call to "Update" */
    template <typename ArgType>
    void DispatchEvent(const string& eventName, const ArgType& value)
    {
        _events.push_back(std::make_unique<TEvent<ArgType>>(eventName, value));
    }

    /** Spaws a new GameObject of the given type into the scene */
    template <class GameObjectType>
    weak_ptr<GameObjectType> Spawn(const string& name = "")
    {
        static_assert(std::is_base_of<GameObject, GameObjectType>::value, "The type given to \"Scene::Spawn<T>()\" must extend \"GameObject\"");
        static_assert(std::is_constructible<GameObjectType, Scene&>::value, "The type given to \"Scene::Spawn<T>()\" must be constructible with a Scene reference");

        // Construct the object and add it to the scene
        auto object = std::make_shared<GameObjectType>(This);
        object->Name = name;

        // Add the object to the scene
        // In an ideal system I would be putting this into a queue that gets pushed in next frame, but unfortunately I don't have time to implement that
        Objects.push_back(object);

        // Add all of the object's components that haven't been explicitly disabled to the scene
        for (auto component : object->GetComponents())
        {
            // Enable it (and initialize it)
            component->Enable();
        }

        return object;
    }

    /** Spawns a distribution of the given type into the scene
    * 'distributionMapPath' - the path to a black and white image representing the likeliness of the object to spawn */
    template <class GameObjectType>
    vector<weak_ptr<GameObjectType>> SpawnDistribution(const string& distributionMapPath, Vec2 offset = Vec2(1, 1), bool alignToTerrain = false)
    {
        vector<weak_ptr<GameObjectType>> result;

        // Attempt to load the image
        QImage distMap;
        distMap.load(distributionMapPath.c_str());

        // Make sure the image loaded correctly
        if (distMap.isNull())
        {
            return result; // No objects were spawned
        }

        for (float x = -1 * MapTerrain.Scale.X; x < 1 * MapTerrain.Scale.X; x += offset.X)
        {
            for (float y = -1 * MapTerrain.Scale.Z; y < 1 * MapTerrain.Scale.Z; y += offset.Y)
            {
                // Figure out where we are on the image, and sample that
                Vec2 terrainPoint = MapTerrain.WorldPointToTerrainPoint(Vec2(x, y));
                Vec2 bitmapPoint = MapTerrain.TerrainPointToBitmapPoint(terrainPoint);

                // Make sure we dont sample outside the image
                bitmapPoint.X = clamp(bitmapPoint.X, 0.f, (float)distMap.width() - 1);
                bitmapPoint.Y = clamp(bitmapPoint.Y, 0.f, (float)distMap.height() - 1);
                float spawnFactor = (float)qRed(distMap.pixel(bitmapPoint.X, bitmapPoint.Y)) / 255;

                // Honestly C++'s random is just so much of a pain in the ass we're gonna use C's "rand()"
                bool spawn = (float)(rand() % 100) / 100 * spawnFactor >= 0.5f;

                // If we determined the object should spawn
                if (spawn)
                {
                    auto object = Spawn<GameObjectType>().lock();
                    object->transform.Position.X = x;
                    object->transform.Position.Z = y;
                    object->SnapToTerrain();

                    if (alignToTerrain)
                    {
                        object->AlignToTerrain();
                    }

                    result.push_back(object);
                }
            }
        }

        // Return collection of spawned objects
        return result;
    }

    /** Returns a collection of all the Components in this scene of the given type */
    template <class ComponentType>
    vector<ComponentType*> GetAllComponentsOfType()
    {
        vector<ComponentType*> result;
        auto componentIndex = _components.find(typeid(ComponentType));

        // If we found components of the desired type
        if (componentIndex != _components.end())
        {
            result.reserve(componentIndex->second.size());
            for (auto component : componentIndex->second)
            {
                result.push_back(static_cast<ComponentType*>(component));
            }
        }

        return result;
    }

    /** Returns a collection of all the Components in this scene of the given type */
    template <class ComponentType>
    vector<const ComponentType*> GetAllComponentsOfType() const
    {
        vector<const ComponentType*> result;
        auto componentIndex = _components.find(typeid(ComponentType));

        // If we found components of the desired type
        if (componentIndex != _components.end())
        {
            result.reserve(componentIndex->second.size());
            for (auto component : componentIndex->second)
            {
                result.push_back(static_cast<const ComponentType*>(component));
            }
        }

        return result;
    }

    /////////////////////
    ///   Operators   ///
public:

    Scene& operator=(const Scene& copy) = delete;
    Scene& operator=(Scene&& move) = delete;

    ////////////////
    ///   Data   ///
private:

    vector<unique_ptr<Event>> _events; // Collection of new events, emptied every update
    unordered_map<type_index, vector<Component*>> _components; // Collection of all components in the scene, sorted by type
    float _time = 0.f; // Current time
};
