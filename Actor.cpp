// Actor.cpp

#include "Actor.h"
#include "Scene.h"

////////////////////////
///   Constructors   ///

Actor::Actor(Scene &scene)
    : GameObject(scene)
{
    scene.eventManager.Bind("Tick", this, &Actor::Tick);
}

//////////////////////////
///   Event Handlers   ///

void Actor::Tick(float /* timeDelta */)
{
    // Do nothing (default)
}
