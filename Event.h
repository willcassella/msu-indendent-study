// Events.h - Copyright 2013-2015 Will Cassella, All Rights Reserved
#pragma once

#include <string>
#include <typeindex>
using std::string;
using std::type_index;

/** Abstract base for events */
class Event
{
    ////////////////////////
    ///   Constructors   ///
protected:

    Event(const string& name, type_index argType);

public:

    virtual ~Event() = default;

    ///////////////////
    ///   Methods   ///
public:

    /** Gets the name of this Event */
    const string& GetName() const;

    /** Returns the type of argument that this Event was created with */
    type_index GetArgType() const;

    ////////////////
    ///   Data   ///
private:

    string _name;
    type_index _argType;
};

/** Template for events that have one parameter */
template <typename ArgType>
class TEvent final : public Event
{
    ////////////////////////
    ///   Constructors   ///
public:

    TEvent(const string& name, const ArgType& value)
        : Event(name, typeid(ArgType)), _value(value)
    {
        // All done
    }

    ///////////////////
    ///   Methods   ///
public:

    /** Returns the value that this event holds */
    const ArgType& GetValue() const
    {
        return _value;
    }

    ////////////////
    ///   Data   ///
private:

    ArgType _value;
};

/** Specialization for events that have no data */
template <>
class TEvent <void> final : public Event
{
    ////////////////////////
    ///   Constructors   ///
public:

    TEvent(const string& name);
};
