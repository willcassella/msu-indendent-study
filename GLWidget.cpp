// GLWidget.cpp

#include <map>
#include <iostream>
#include <fstream>
#include <QGLWidget>
#include <QKeyEvent>
#include <WillowMath/Vec4.h>
#include "GLWidget.h"
#include "Levels.h"
#include "GameObject.h"
#include "CameraComponent.h"
#include "StaticMeshComponent.h"
#include "LightComponent.h"
using std::map;
using std::ifstream;

/////////////////////
///   Functions   ///

/** Parses an equality in the form of "A = B" */
pair<string, string> ParseEquality(const string& str)
{
    auto index = str.find('=');

    string a = str.substr(0, index - 1);
    string b = str.substr(index + 2);

    return pair<string, string>(a, b);
}

////////////////////////
///   Constructors   ///

GLWidget::GLWidget(QWidget* parent)
    : QOpenGLWidget(parent), _timer(this)
{
    // Insert null table elements
    _meshes[""] = Mesh(); // Null mesh
    _materials[""] = Material(); // Null material
    _textures[""] = 0;
    _shaders[""] = 0;

    // Load the forest scene
    _scene = LoadForest();

    // Hook up tick event
    connect(&_timer, SIGNAL(timeout()), this, SLOT(Tick()));
    _timer.start(16);
}

GLWidget::~GLWidget()
{
    // Clean up scene
    delete _scene;

    // Clean up StaticMeshComponent data
    clearTextures();
    clearShaders();
    clearMeshes();
    clearMaterials();
    glDeleteBuffers(1, &_instanceBuffer);

    // Clean up terrain
    glDeleteProgram(_terrainProgram);
    glDeleteBuffers(1, &_terrainVAO);
    glDeleteBuffers(1, &_terrainBuffer);
    glDeleteTextures(1, &_terrainHeightMap);
}

/////////////////
///   Slots   ///

void GLWidget::Tick()
{
    Vec3 movement;

    // Check input
    if (_forward)
        movement.Z -= 1;
    if (_backward)
        movement.Z += 1;
    if (_left)
        movement.X -= 1;
    if (_right)
        movement.X += 1;

    if (_shift)
        _scene->DispatchEvent("Boost");
    _scene->DispatchEvent("Move", movement);

    _scene->Update(0.016f);
    update();
}

///////////////////
///   Methods   ///

void GLWidget::initializeGL()
{
    // Initialize general OpenGL
    initializeOpenGLFunctions();
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

    // Initialize the instance buffer
    glGenBuffers(1, &_instanceBuffer);

    // Initialize the terrain
    initializeTerrain();

    // Set up gBuffer
    glGenFramebuffers(1, &_gBuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, _gBuffer);

    // Set up depth component for gBuffer
    glGenTextures(1, &_depthBuffer);
    glBindTexture(GL_TEXTURE_2D, _depthBuffer);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT32, width(), height(), 0, GL_DEPTH_COMPONENT, GL_UNSIGNED_INT, 0); // 1 32-bit unsigned integer component for depth
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, _depthBuffer, 0);

    // Create a position buffer for the GBuffer
    glGenTextures(1, &_positionBuffer);
    glBindTexture(GL_TEXTURE_2D, _positionBuffer);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32F, width(), height(), 0, GL_RGB, GL_FLOAT, 0); // 3 32-bit floating point components for position
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, _positionBuffer, 0);

    // Create a normal buffer for the GBuffer
    glGenTextures(1, &_normalBuffer);
    glBindTexture(GL_TEXTURE_2D, _normalBuffer);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32F, width(), height(), 0, GL_RGB, GL_FLOAT, 0); // 3 32-bit floating point components for normal
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, _normalBuffer, 0);

    // Create a diffuse buffer for the GBuffer
    glGenTextures(1, &_diffuseBuffer);
    glBindTexture(GL_TEXTURE_2D, _diffuseBuffer);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width(), height(), 0, GL_RGBA, GL_UNSIGNED_INT, 0); // 3 8-bit normalized integer components for color, 8-bits for luminance
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, GL_TEXTURE_2D, _diffuseBuffer, 0);

    // Create a specular buffer for the GBuffer
    glGenTextures(1, &_specularBuffer);
    glBindTexture(GL_TEXTURE_2D, _specularBuffer);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width(), height(), 0, GL_RED, GL_UNSIGNED_INT, 0); // 4 8-bit noramlized integer components for specular
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT3, GL_TEXTURE_2D, _specularBuffer, 0);

    // Make sure the GBuffer was constructed successfully
    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
        std::cout << "Error creating the GBuffer" << std::endl;
    }

    // Create and upload a shader program for the screen quad
    GLuint vShader = loadShader("shaders/viewport.vert", GL_VERTEX_SHADER);
    GLuint fShader = loadShader("shaders/viewport.frag", GL_FRAGMENT_SHADER);
    _screenProgram = glCreateProgram();
    glAttachShader(_screenProgram, vShader);
    glAttachShader(_screenProgram, fShader);
    glBindAttribLocation(_screenProgram, 0, "vPosition");
    glBindAttribLocation(_screenProgram, 1, "vTexCoord");
    glLinkProgram(_screenProgram);

    // Clean up the shaders
    glDetachShader(_screenProgram, vShader);
    glDetachShader(_screenProgram, fShader);
    glDeleteShader(vShader);
    glDeleteShader(fShader);

    // Create a VAO for screen quad
    glGenVertexArrays(1, &_screenVAO);
    glBindVertexArray(_screenVAO);

    // Create and upload a VBO for screen quad
    glGenBuffers(1, &_screenVBO);
    glBindBuffer(GL_ARRAY_BUFFER, _screenVBO);
    float screenQuad[] = {
        -1.f,  1.f, 0.f, 1.f, // Top-left point
        -1.f, -1.f, 0.f, 0.f, // Bottom-left point
         1.f, -1.f, 1.f, 0.f, // Bottom-right point
         1.f,  1.f, 1.f, 1.f  // Top-right point
    };
    glBufferData(GL_ARRAY_BUFFER, sizeof(screenQuad), screenQuad, GL_STATIC_DRAW);

    // Specify vertex attributes
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(0, 2, GL_FLOAT, false, sizeof(float) * 4, 0);                          // "vPosition" attribute
    glVertexAttribPointer(1, 2, GL_FLOAT, false, sizeof(float) * 4, (void*)(sizeof(float) * 2)); // "vTexCoord" attribute

    // Upload buffer variables
    glUseProgram(_screenProgram);
    glUniform1i(glGetUniformLocation(_screenProgram, "positionBuffer"), 0);
    glUniform1i(glGetUniformLocation(_screenProgram, "normalBuffer"), 1);
    glUniform1i(glGetUniformLocation(_screenProgram, "diffuseBuffer"), 2);
    glUniform1i(glGetUniformLocation(_screenProgram, "specularBuffer"), 3);
}

void GLWidget::resizeGL(int width, int height)
{
    _scene->DispatchEvent("Resize", Vec2(width, height));

    // Update framebuffer size
    glBindTexture(GL_TEXTURE_2D, _depthBuffer);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT32, width, height, 0, GL_DEPTH_COMPONENT, GL_UNSIGNED_INT, 0);

    glBindTexture(GL_TEXTURE_2D, _positionBuffer);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32F, width, height, 0, GL_RGB, GL_FLOAT, 0);

    glBindTexture(GL_TEXTURE_2D, _normalBuffer);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32F, width, height, 0, GL_RGB, GL_FLOAT, 0);

    glBindTexture(GL_TEXTURE_2D, _diffuseBuffer);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0, GL_RGBA, GL_UNSIGNED_INT, 0);

    glBindTexture(GL_TEXTURE_2D, _specularBuffer);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0, GL_RED, GL_UNSIGNED_INT, 0);
}

void GLWidget::paintGL()
{
    // Get the framebuffer that QT set for us
    GLint defaultFBO;
    glGetIntegerv(GL_FRAMEBUFFER_BINDING, &defaultFBO);

    // Bind the GBuffer and its sub-buffers for drawing
    glBindFramebuffer(GL_FRAMEBUFFER, _gBuffer);
    GLuint drawBuffers[] = { GL_COLOR_ATTACHMENT0 /* position */, GL_COLOR_ATTACHMENT1 /* normal */, GL_COLOR_ATTACHMENT2 /* diffuse */, GL_COLOR_ATTACHMENT3 /* specular */};
    glDrawBuffers(4, drawBuffers);

    // Clear the GBuffer
    glEnable(GL_DEPTH_TEST);
    glDisable(GL_BLEND);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // Get scene data
    auto cameras = _scene->GetAllComponentsOfType<CameraComponent>();
    auto staticMeshComponents = _scene->GetAllComponentsOfType<StaticMeshComponent>();
    auto lights = _scene->GetAllComponentsOfType<LightComponent>();
    float time = _scene->GetTime();

    // If there are no cameras in the scene
    if (cameras.empty())
        return;

    // Use the first camera
    Mat4 projection = cameras.front()->GetProjectionMatrix();
    Mat4 camModel = cameras.front()->GetOwner().GetModelMatrix();
    Mat4 view = camModel.Inverse();
    Vec3 camPos = camModel * Vec3(0, 0, 0);

    // Render the terrain
    renderTerrain(view, projection);

    // Build collection of draw orders
    unordered_map<const Material*, unordered_map<const Mesh*, vector<Mat4>>> drawOrders;

    // Populate the collection of draw orders
    for (auto smc : staticMeshComponents)
    {
        drawOrders[&getMaterial(smc->Material)][&getMesh(smc->Mesh)].push_back(smc->GetOwner().GetModelMatrix());
    }

    // Bind the instance buffer (so that we don't have to do it for every mesh)
    glBindBuffer(GL_ARRAY_BUFFER, _instanceBuffer);

    // For each material being rendered
    for (const auto& material : drawOrders)
    {
        // Bind the material
        glUseProgram(material.first->ID);

        glUniform1f(glGetUniformLocation(material.first->ID, "time"), time);
        glUniformMatrix4fv(glGetUniformLocation(material.first->ID, "projection"), 1, false, (float*)&projection);
        glUniformMatrix4fv(glGetUniformLocation(material.first->ID, "view"), 1, false, (float*)&view);

        // Bind the material's textures
        uint32 i = 0;
        for (const auto& textureVar : material.first->Textures)
        {
            glActiveTexture(GL_TEXTURE0 + i);
            glBindTexture(GL_TEXTURE_2D, textureVar.second);
            glUniform1i(textureVar.first, i);
            ++i;
        }

        // For each mesh using that material
        for (const auto& mesh : material.second)
        {
            // Bind the mesh
            glBindVertexArray(mesh.first->VAO);

            // Upload instance matrices
            glBufferData(GL_ARRAY_BUFFER, sizeof(Mat4) * mesh.second.size(), &mesh.second.front(), GL_STREAM_DRAW);

            // Draw
            glDrawElementsInstanced(GL_TRIANGLES, mesh.first->NumElements, GL_UNSIGNED_INT, 0, (GLsizei)mesh.second.size());
        }
    }

    // Bind the default framebuffer for drawing
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFBO);

    // Clear the frame
    glEnable(GL_BLEND);
    glDisable(GL_DEPTH_TEST);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE);
    glClear(GL_COLOR_BUFFER_BIT);

    // Bind the screen quad
    glBindVertexArray(_screenVAO);
    glUseProgram(_screenProgram);

    // Upload the camera's position
    glUniform3fv(glGetUniformLocation(_screenProgram, "camPos"), 1, (float*)&camPos);

    // Bind the GBuffer's sub-buffers as textures for reading
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, _positionBuffer);
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, _normalBuffer);
    glActiveTexture(GL_TEXTURE2);
    glBindTexture(GL_TEXTURE_2D, _diffuseBuffer);
    glActiveTexture(GL_TEXTURE3);
    glBindTexture(GL_TEXTURE_2D, _specularBuffer);

    // Get the starting index for the light array
    // LOOK AT THIS BULLSHIT
    // lightIndex is "4" while colorIndex is "3"
    // EVEN THOUGH THEY ARE DECLARED IN THE OPPOSITE ORDER
    // WHHAT THE FUUUUUUCCK
    // IT EVEN SAYS THEY SHOULD BE IN THE ORDER THEY'RE DECLARED HERE: https://www.opengl.org/wiki/Uniform_%28GLSL%29
    GLint lightIndex = glGetUniformLocation(_screenProgram, "lights[0].position");
    GLint colorIndex = glGetUniformLocation(_screenProgram, "lights[0].color");

    // Pre-sort lighting
    typedef tuple<float, float, float, float> LightVec;
    Vec4 totalAmbient = Vec4(0, 0, 0, 1);
    vector<pair<Vec4, Vec4>> lightCollection;

    // Iterate through LightComponents
    for (auto light : lights)
    {
        Vec3 ownerPos = light->GetOwner().GetModelMatrix() * Vec3(0, 0, 0);

        switch(light->type)
        {
        case LightComponent::Type::AMBIENT:
            totalAmbient += Vec4(light->Color.X / light->Intensity, light->Color.Y / light->Intensity, light->Color.Z / light->Intensity, 0);
            break;
        case LightComponent::Type::DIRECTIONAL:
            lightCollection.push_back(pair<Vec4, Vec4>(
                        Vec4( -ownerPos.X, -ownerPos.Y, -ownerPos.Z, 0 ),
                        Vec4( light->Color.X, light->Color.Y, light->Color.Z, light->Intensity )
            ));
            break;
        case LightComponent::Type::SPOT:
            lightCollection.push_back(pair<Vec4, Vec4>(
                        Vec4( ownerPos.X, ownerPos.Y, ownerPos.Z, 1 ),
                        Vec4( light->Color.X, light->Color.Y, light->Color.Z, light->Intensity )
            ));
            break;
        }
    }

    // Aggregate lighting
    for (uint32 i = 0; i < lightCollection.size(); i += 10)
    {
        // Upload ambient light accumulation
        glUniform4fv(glGetUniformLocation(_screenProgram, "ambientLight"), 1, (float*)&totalAmbient);

        // Upload individual lights
        uint32 numLights = 0;
        for (uint32 j = i; j < i + 10 && j < lightCollection.size(); ++j, ++numLights)
        {
            // Upload position
            glUniform4fv(lightIndex + numLights * 2, 1, (float*)&lightCollection[j].first);

            // Upload color
            glUniform4fv(colorIndex + numLights * 2, 1, (float*)&lightCollection[j].second);
        }

        // Upload the number of lights in this batch
        glUniform1i(glGetUniformLocation(_screenProgram, "numLights"), numLights);

        // Draw the screen quad
        glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

        // Disable ambient after first pass
        totalAmbient.W = 0;
    }

    // Clean up
    glBindVertexArray(0);
}

void GLWidget::mousePressEvent(QMouseEvent* event)
{
    _lastPoint = Vec2(event->x(), event->y());
    _scene->DispatchEvent("Click", _lastPoint);
}

void GLWidget::mouseMoveEvent(QMouseEvent* event)
{
    Vec2 newPoint(event->x(), event->y());
    _scene->DispatchEvent("Look", newPoint - _lastPoint);
    _lastPoint = newPoint;
}

void GLWidget::keyPressEvent(QKeyEvent* event)
{
    switch(event->key())
    {
    case Qt::Key_W:
        _forward = true;
        break;
    case Qt::Key_S:
        _backward = true;
        break;
    case Qt::Key_A:
        _left = true;
        break;
    case Qt::Key_D:
        _right = true;
        break;
    case Qt::Key_Shift:
        _shift = true;
        break;
    case Qt::Key_Escape:
        close();
        break;
    case Qt::Key_F5:
        // Reload all textures
        for (const auto& texture : _textures)
        {
            loadTexture(texture.second, texture.first);
        }
        loadTexture(_terrainHeightMap, _terrainHeightMapPath);
        _scene->MapTerrain.ForceReloadHeightMap();
        _scene->DispatchEvent("TerrainChanged");
        break;
    }
}

void GLWidget::keyReleaseEvent(QKeyEvent* event)
{
    switch(event->key())
    {
    case Qt::Key_W:
        _forward = false;
        break;
    case Qt::Key_S:
        _backward = false;
        break;
    case Qt::Key_A:
        _left = false;
        break;
    case Qt::Key_D:
        _right = false;
        break;
    case Qt::Key_Shift:
        _shift = false;
        break;
    }
}

void GLWidget::loadTexture(GLuint buffer, const string& path)
{
    // Load image into memory
    QImage texture;
    texture.load(path.c_str());

    // Make sure the image was loaded sucessfully
    if (texture.isNull())
    {
        // Load an empty texture
        glBindTexture(GL_TEXTURE_2D, buffer);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 0, 0, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
    }
    else
    {
        // Buffer image data into OpenGL
        texture = QGLWidget::convertToGLFormat(texture);
        glBindTexture(GL_TEXTURE_2D, buffer);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB10_A2, texture.width(), texture.height(), 0, GL_RGBA, GL_UNSIGNED_BYTE, texture.bits());
        glGenerateMipmap(GL_TEXTURE_2D);
    }
}

GLuint GLWidget::getTexture(const string& path)
{
    // Search for the texture in the texture table
    auto textureIndex = _textures.find(path);

    if (textureIndex != _textures.end())
    {
        // Return the found texture
        return textureIndex->second;
    }
    else
    {
        // Create a new texture
        GLuint texture;
        glGenTextures(1, &texture);
        glBindTexture(GL_TEXTURE_2D, texture);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 8);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_LINEAR);
        //glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, 8.f);
        loadTexture(texture, path);

        // Put it into the texture table
        _textures[path] = texture;

        return texture;
    }
}

void GLWidget::clearTextures()
{
    for (auto& texture : _textures)
    {
        glDeleteTextures(1, &texture.second);
    }

    _textures.clear();
}

GLuint GLWidget::loadShader(const string& path, GLenum type)
{
    // Open the shader file
    ifstream file;
    file.open(path, std::ios::in);
    string line, source;

    // Read the file
    if (file.is_open())
    {
        while (std::getline(file, line))
        {
            source += line;
            source += '\n';
        }
    }
    else
    {
        std::cout << "Shader failed to load: " << path << std::endl;
        return 0; // Null shader
    }

    // We are done with the file
    file.close();
    const char* source_cstr = source.c_str();

    // Create and compile the shader
    GLuint shader = glCreateShader(type);
    glShaderSource(shader, 1, &source_cstr, NULL);
    glCompileShader(shader);

    // Make sure the shader was sucessfully compiled
    GLint compiled;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);
    if (!compiled)
    {
        GLsizei length;
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &length);

        GLchar* log = new GLchar[length+1];
        glGetShaderInfoLog(shader, length, &length, log);
        std::cout << "Compiling shader \"" << path << "\" failed: " << log << std::endl;
        delete[] log;
    }

    return shader;
}

GLuint GLWidget::getShader(const string& path, GLenum type)
{
    // Search for the shader in the shader table
    auto shaderIndex = _shaders.find(path);

    if (shaderIndex != _shaders.end())
    {
        // Return the found shader
        return shaderIndex->second;
    }
    else
    {
        // Create a new shader and put it into the shader table
        GLuint shader = loadShader(path, type);
        _shaders[path] = shader;

        return shader;
    }
}

void GLWidget::clearShaders()
{
    for (const auto& shader : _shaders)
    {
        glDeleteShader(shader.second);
    }

    _shaders.clear();
}

GLWidget::Mesh GLWidget::loadMesh(const string& path)
{
    // Attempt to open the file
    ifstream file;
    file.open(path, std::ios::binary | std::ios::in);

    // Make sure the file opened successfully
    if (!file.is_open())
    {
        return Mesh(); // Null mesh
    }

    // Get the number of vertices
    uint32 numVerts;
    file.read((char*)&numVerts, sizeof(uint32));

    // Read vertices
    char* vertices = new char[numVerts * sizeof(float) * 8]; // A vertex consists of 8 floating point numbers
    file.read(vertices, numVerts * sizeof(float) * 8);

    // Get the number of elements
    uint32 numElems;
    file.read((char*)&numElems, sizeof(uint32));

    // Read elements
    uint32* elements = new uint32[numElems];
    file.read((char*)elements, numElems * sizeof(uint32));

    // We are done with the file
    file.close();

    // Create a mesh object
    Mesh mesh;
    mesh.NumElements = numElems;

    // Create VAO for mesh
    glGenVertexArrays(1, &mesh.VAO);
    glBindVertexArray(mesh.VAO);

    // Create vertex buffer for mesh
    glGenBuffers(1, &mesh.VertexBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, mesh.VertexBuffer);

    // Upload data to vertex buffer
    glBufferData(GL_ARRAY_BUFFER, numVerts * sizeof(float) * 8, vertices, GL_STATIC_DRAW);
    delete[] vertices;

    // Create element buffer for mesh
    GLuint elementBuffer;
    glGenBuffers(1, &elementBuffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementBuffer);

    // Upload data to element buffer
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, numElems * sizeof(uint32), elements, GL_STATIC_DRAW);
    delete[] elements;

    // Set up vertex attributes
    glEnableVertexAttribArray(0); // Location of "vPosition" attribute
    glEnableVertexAttribArray(1); // Location of "vNormal" attribute
    glEnableVertexAttribArray(2); // Location of "vCoord" attribute
    glEnableVertexAttribArray(3); // Location of first vector of "model" attribute
    glEnableVertexAttribArray(4); // Location of second vector of "model" attribute
    glEnableVertexAttribArray(5); // Location of third vector of "model" attribute
    glEnableVertexAttribArray(6); // Location of fourth vector of "model" attribute
    glVertexAttribDivisor(3, 1); // First vector of model matrix is updated per-instance
    glVertexAttribDivisor(4, 1); // Second vector of model matrix is updated per-instance
    glVertexAttribDivisor(5, 1); // Third vector of model matrix is updated per-instance
    glVertexAttribDivisor(6, 1); // Fourth vector of model matrix is updated per-instance

    // Describe per-vertex attributes
    // The VBO for per-vertex attributes is already bound (mesh.VertexBuffer)
    glVertexAttribPointer(0, 3, GL_FLOAT, false, sizeof(float) * 8, 0); // "vPosition" attribute
    glVertexAttribPointer(1, 3, GL_FLOAT, true, sizeof(float) * 8, (void*)(sizeof(float) * 5)); // "vNormal" attribute
    glVertexAttribPointer(2, 2, GL_FLOAT, false, sizeof(float) * 8, (void*)(sizeof(float) * 3)); // "vCoord" attribute

    // Describe per-instance attributes
    glBindBuffer(GL_ARRAY_BUFFER, _instanceBuffer);
    glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, sizeof(Mat4), 0); // First vector of "model" attribute
    glVertexAttribPointer(4, 4, GL_FLOAT, GL_FALSE, sizeof(Mat4), (void*)(sizeof(float) * 4)); // Second vector of "model" attribute
    glVertexAttribPointer(5, 4, GL_FLOAT, GL_FALSE, sizeof(Mat4), (void*)(sizeof(float) * 8)); // Third vector of "model" attribute
    glVertexAttribPointer(6, 4, GL_FLOAT, GL_FALSE, sizeof(Mat4), (void*)(sizeof(float) * 12)); // Fourth vector of "model" attribute

    return mesh;
}

const GLWidget::Mesh& GLWidget::getMesh(const string& path)
{
    // Search for the mesh in the mesh table
    auto meshIndex = _meshes.find(path);

    if (meshIndex != _meshes.end())
    {
        // Return the found mesh
        return meshIndex->second;
    }
    else
    {
        // Create a new mesh and put it into the material table
        Mesh mesh = loadMesh(path);
        _meshes[path] = mesh;
        return _meshes[path];
    }
}

void GLWidget::clearMeshes()
{
    for (auto& mesh : _meshes)
    {
        glDeleteBuffers(1, &mesh.second.VAO);
        glDeleteBuffers(1, &mesh.second.VertexBuffer);
        glDeleteBuffers(1, &mesh.second.ElementBuffer);
    }

    _meshes.clear();
}

GLWidget::Material GLWidget::loadMaterial(const string& path)
{
    // Open the material file
    ifstream file;
    file.open(path, std::ios::in);

    // Make sure the file opened sucessfully
    if (!file.is_open())
    {
        std::cout << "Could not load material: " << path << std::endl;
        return Material(); // Null material
    }

    // Table of texture variables
    map<string, string> textureVars;
    string vShaderPath;
    string fShaderPath;

    // Read the file
    string line;
    while (std::getline(file, line))
    {
        // Parse shaders
        if (line == "[SHADERS]")
        {
            for (std::getline(file, line); !line.empty(); std::getline(file, line))
            {
                auto shader = ParseEquality(line);
                if (shader.first == "VertexShader")
                {
                    vShaderPath = shader.second;
                }
                else if (shader.first == "FragmentShader")
                {
                    fShaderPath = shader.second;
                }
            }
        }
        // Parse textures
        else if (line == "[TEXTURES]")
        {
            for (std::getline(file, line); !line.empty(); std::getline(file, line))
            {
                auto texture = ParseEquality(line);
                textureVars[texture.first] = texture.second;
            }
        }
    }

    // We are done with the file
    file.close();

    Material material;

    // Generate an OpenGL material
    material.ID = glCreateProgram();
    GLuint vShader = getShader(vShaderPath, GL_VERTEX_SHADER);
    GLuint fShader = getShader(fShaderPath, GL_FRAGMENT_SHADER);
    glAttachShader(material.ID, vShader);
    glAttachShader(material.ID, fShader);
    glBindAttribLocation(material.ID, 0, "vPosition");
    glBindAttribLocation(material.ID, 1, "vNormal");
    glBindAttribLocation(material.ID, 2, "vCoord");
    glBindAttribLocation(material.ID, 3, "model");
    glLinkProgram(material.ID);

    // Detach the shaders now that the program has been created
    glDetachShader(material.ID, vShader);
    glDetachShader(material.ID, fShader);

    // Make sure the program linked successfully
    GLint linked;
    glGetProgramiv(material.ID, GL_LINK_STATUS, &linked);
    if (!linked)
    {
        GLsizei length;
        glGetProgramiv(material.ID, GL_INFO_LOG_LENGTH, &length);

        GLchar* log = new GLchar[length+1];
        glGetProgramInfoLog(material.ID, length, &length, log);
        std::cout << "Compiling material \"" << path << "\" failed: " << log << std::endl;
        delete[] log;
    }

    // Resolve all texture variables
    for (const auto& textureVar : textureVars)
    {
        material.Textures.push_back(std::make_pair(glGetUniformLocation(material.ID, textureVar.first.c_str()), getTexture(textureVar.second)));
    }

    return material;
}

const GLWidget::Material& GLWidget::getMaterial(const string& path)
{
    // Search for the material in the material table
    auto materialIndex = _materials.find(path);

    if (materialIndex != _materials.end())
    {
        // Return the found material
        return materialIndex->second;
    }
    else
    {
        // Create a new material and put it into the material table
        Material material = loadMaterial(path);
        _materials[path] = material;
        return _materials[path];
    }
}

void GLWidget::clearMaterials()
{
    for (const auto& material : _materials)
    {
        glDeleteProgram(material.second.ID);
    }

    _materials.clear();
}

void GLWidget::initializeTerrain()
{
    // Create a new Vertex Array Object on the GPU which
    // saves the attribute layout of our vertices.
    glGenVertexArrays(1, &_terrainVAO);
    glBindVertexArray(_terrainVAO);

    // Create a buffer on the GPU for position data
    glGenBuffers(1, &_terrainBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, _terrainBuffer);

    // Load the terrain shader program
    GLuint vShader = loadShader("shaders/terrain.vert", GL_VERTEX_SHADER);
    GLuint fShader = loadShader("shaders/terrain.frag", GL_FRAGMENT_SHADER);
    _terrainProgram = glCreateProgram();
    glAttachShader(_terrainProgram, vShader);
    glAttachShader(_terrainProgram, fShader);
    glLinkProgram(_terrainProgram);
    glUseProgram(_terrainProgram);

    // Make sure the program linked successfully
    GLint linked;
    glGetProgramiv(_terrainProgram, GL_LINK_STATUS, &linked);
    if (!linked)
    {
        GLsizei length;
        glGetProgramiv(_terrainProgram, GL_INFO_LOG_LENGTH, &length);

        GLchar* log = new GLchar[length+1];
        glGetProgramInfoLog(_terrainProgram, length, &length, log);
        std::cout << "Terrain material compilation failed: " << log << std::endl;
        delete[] log;
    }

    // Detach and delete the shaders now that the program works
    glDetachShader(_terrainProgram, vShader);
    glDetachShader(_terrainProgram, fShader);
    glDeleteShader(vShader);
    glDeleteShader(fShader);

    // Set constant shader uniforms
    glUniform1i(glGetUniformLocation(_terrainProgram, "heightMap"), 0);
    glUniform1i(glGetUniformLocation(_terrainProgram, "texture1"), 1);
    glUniform1i(glGetUniformLocation(_terrainProgram, "texture2"), 2);
    glUniform1i(glGetUniformLocation(_terrainProgram, "texture3"), 3);
    glUniform1i(glGetUniformLocation(_terrainProgram, "texture4"), 4);
    glUniform1i(glGetUniformLocation(_terrainProgram, "textureMask"), 5);
    glUniform1i(glGetUniformLocation(_terrainProgram, "specular"), 6);

    // Bind the position attribute
    GLint positionIndex = glGetAttribLocation(_terrainProgram, "vPosition");
    glEnableVertexAttribArray(positionIndex);
    glVertexAttribPointer(positionIndex, 2, GL_FLOAT, GL_FALSE, sizeof(Vec2) * 2, 0);

    // Bind the coordinate attribute
    GLint coordinateIndex = glGetAttribLocation(_terrainProgram, "vTexCoord");
    glEnableVertexAttribArray(coordinateIndex);
    glVertexAttribPointer(coordinateIndex, 2, GL_FLOAT, GL_FALSE, sizeof(Vec2) * 2, (void*)sizeof(Vec2));

    // Set up the heightmap texture
    glGenTextures(1, &_terrainHeightMap);
    glBindTexture(GL_TEXTURE_2D, _terrainHeightMap);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    // Clean up
    glBindVertexArray(0);

    // Generate the actual terrain data
    generateTerrain();
}

void GLWidget::generateTerrain()
{
    // Add all points for the mesh
    vector<Vec2> points;

    uint32 resolution = _terrainNumPatches + 1;

    for (uint32 y = 0; y < _terrainNumPatches; ++y)
    {
        for (uint32 x = 0; x < _terrainNumPatches + 1; ++x)
        {
            float xPos = 2.f / resolution * x - 1;
            float yPos = 2.f / resolution * y - 1;
            float xTex = 1.f / resolution * x;
            float yTex = 1.f / resolution * y;
            float yPos2 = 2.f / resolution * (y + 1) - 1;
            float yTex2 = 1.f / resolution * (y + 1);

            points.push_back(Vec2(xPos, yPos)); // First position
            points.push_back(Vec2(xTex, yTex)); // First texture coordinate
            points.push_back(Vec2(xPos, yPos2)); // Second position
            points.push_back(Vec2(xTex, yTex2)); // Second texture coordinate
        }
    }

    // Upload the position data to the GPU, storing
    // it in the buffer we just allocated.
    glBindBuffer(GL_ARRAY_BUFFER, _terrainBuffer);
    glBufferData(GL_ARRAY_BUFFER, points.size() * sizeof(Vec2), &points.front(), GL_STATIC_DRAW);
}

void GLWidget::renderTerrain(const Mat4& view, const Mat4& projection)
{
    // Make sure the heightMap hasn't changed
    if (_scene->MapTerrain.GetHeightMapPath() != _terrainHeightMapPath)
    {
        // Load the new heightmap
        _terrainHeightMapPath = _scene->MapTerrain.GetHeightMapPath();
        loadTexture(_terrainHeightMap, _terrainHeightMapPath);
    }

    // Make sure the number of terrain patches hasn't changed
    if (_scene->MapTerrain.NumPatches != _terrainNumPatches)
    {
        // Re-generate the terrain
        _terrainNumPatches = _scene->MapTerrain.NumPatches;
        generateTerrain();
    }

    // Bind variables
    glUseProgram(_terrainProgram);
    glBindVertexArray(_terrainVAO);
    glUniformMatrix4fv(glGetUniformLocation(_terrainProgram, "projection"), 1, false, (float*)&projection);
    glUniformMatrix4fv(glGetUniformLocation(_terrainProgram, "view"), 1, false, (float*)&view);
    glUniform3fv(glGetUniformLocation(_terrainProgram, "scale"), 1, (float*)&_scene->MapTerrain.Scale);
    glUniform1f(glGetUniformLocation(_terrainProgram, "patchSize"), _scene->MapTerrain.GetPatchSize());
    glUniform1f(glGetUniformLocation(_terrainProgram, "texture1Size"), _scene->MapTerrain.Texture1Size);
    glUniform1f(glGetUniformLocation(_terrainProgram, "texture2Size"), _scene->MapTerrain.Texture2Size);
    glUniform1f(glGetUniformLocation(_terrainProgram, "texture3Size"), _scene->MapTerrain.Texture3Size);
    glUniform1f(glGetUniformLocation(_terrainProgram, "texture4Size"), _scene->MapTerrain.Texture4Size);

    // Bind textures
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, _terrainHeightMap);
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, getTexture(_scene->MapTerrain.Texture1));
    glActiveTexture(GL_TEXTURE2);
    glBindTexture(GL_TEXTURE_2D, getTexture(_scene->MapTerrain.Texture2));
    glActiveTexture(GL_TEXTURE3);
    glBindTexture(GL_TEXTURE_2D, getTexture(_scene->MapTerrain.Texture3));
    glActiveTexture(GL_TEXTURE4);
    glBindTexture(GL_TEXTURE_2D, getTexture(_scene->MapTerrain.Texture4));
    glActiveTexture(GL_TEXTURE5);
    glBindTexture(GL_TEXTURE_2D, getTexture(_scene->MapTerrain.TextureMask));
    glActiveTexture(GL_TEXTURE6);
    glBindTexture(GL_TEXTURE_2D, getTexture(_scene->MapTerrain.Specular));

    // Build the arrays of "first" and "count" parameters for each draw call
    GLint* firstArray = new GLint[_terrainNumPatches];
    GLsizei* countArray = new GLsizei[_terrainNumPatches];
    uint32 resolution = (_terrainNumPatches + 1) * 2;

    for (uint32 i = 0; i < _terrainNumPatches; ++i)
    {
        firstArray[i] = i * resolution;
        countArray[i] = resolution;
    }

    // Draw each strip of the terrain (with one draw call!)
    glMultiDrawArrays(GL_TRIANGLE_STRIP, firstArray, countArray, _terrainNumPatches);

    // Clean up
    glBindVertexArray(0);
    delete[] firstArray;
    delete[] countArray;
}
