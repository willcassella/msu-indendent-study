// viewport.frag
#version 330 core

struct Light
{
	vec4 position; // xyz direction + w directional/spotlight specifier
	vec4 color; // rgb color + a intensity
};

uniform sampler2D positionBuffer;
uniform sampler2D normalBuffer;
uniform sampler2D diffuseBuffer;
uniform sampler2D specularBuffer;
uniform vec3 camPos;
uniform vec4 ambientLight;
uniform Light lights[10];
uniform int numLights;

in vec2 fTexCoord;

out vec4 outColor;

void main()
{
	// Extract surface data from gBuffer
	vec3 surfacePosition = texture(positionBuffer, fTexCoord).xyz;
	vec3 surfaceNormal = normalize(texture(normalBuffer, fTexCoord).xyz);
	vec3 surfaceColor = texture(diffuseBuffer, fTexCoord).rgb;
	float surfaceLuminance = texture(diffuseBuffer, fTexCoord).a;
	vec4 surfaceSpecular = texture(specularBuffer, fTexCoord);
	
	// Calculate per-pixel constant vectors
	vec3 surfaceToCamera = normalize(camPos - surfacePosition);
	
	// Initial values for lighting components
	vec3 finalAmbient;
	vec3 finalDiffuse;
	vec3 finalSpecular;
	
	// Calculate ambient lighting
	if (ambientLight.w != 0)
	{
		finalAmbient = (ambientLight.rgb * surfaceColor) + (surfaceColor * surfaceLuminance);
	}
	else
	{
		// We are not doing the ambient pass
		finalAmbient = vec3(0, 0, 0);
	}
	
	// Calculate diffuse and specular for each light
	for (int i = 0; i < numLights; ++i)
	{
		Light light = lights[i];
	
		// Initial values for light color, direction, and attenuation
		vec3 lightColor = vec3(light.color.rgb);
		vec3 surfaceToLight;
		float attenuation = 1;
		
		// If we have a directional light
		if (light.position.w == 0)
		{
			surfaceToLight = normalize(-light.position.xyz);
			attenuation = light.color.w;
		}
		else
		{
			// We have a spotlight
			surfaceToLight = normalize(light.position.xyz - surfacePosition);
			attenuation = 1.0 / length(light.position.xyz - surfacePosition) * light.color.w;
		}
		
		// Calculate diffuse lighting
		float diffuseIntensity = max(dot(surfaceNormal, surfaceToLight), 0);
		vec3 diffuse = (surfaceColor * lightColor) * diffuseIntensity * attenuation;
		
		vec3 specular;
		if (diffuseIntensity == 0)
		{
			// We have no specularity
			specular = vec3(0, 0, 0);
		}
		else
		{
			// Calculate specularity
			vec3 surfaceReflect = normalize(reflect(-surfaceToLight, surfaceNormal));
			float specularIntensity = pow(max(dot(surfaceReflect, surfaceToCamera), 0), surfaceSpecular.w * 100);
			specular = surfaceSpecular.rgb * lightColor * specularIntensity * attenuation;
		}
		
		// Composite lighting results with final results
		finalDiffuse += diffuse;
		finalSpecular += specular;
	}
	
	// Composite final lighting result
	outColor = vec4(finalAmbient + finalDiffuse + finalSpecular, 1);
}
