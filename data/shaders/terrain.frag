// terrain.frag
#version 330 core

uniform sampler2D heightMap;
uniform sampler2D texture1;
uniform sampler2D texture2;
uniform sampler2D texture3;
uniform sampler2D texture4;
uniform sampler2D textureMask;
uniform sampler2D specular;

uniform float texture1Size;
uniform float texture2Size;
uniform float texture3Size;
uniform float texture4Size;

in vec3 fPosition;
in vec3 fNormal;
in vec2 fTexCoord;

layout (location = 0) out vec3 outPosition;
layout (location = 1) out vec3 outNormal;
layout (location = 2) out vec4 outDiffuse;
layout (location = 3) out vec4 outSpecular;

void main()
{
	vec4 mask = texture(textureMask, fTexCoord);
	vec3 tex1 = texture(texture1, fTexCoord * texture1Size).rgb * mask.r;
	vec3 tex2 = texture(texture2, fTexCoord * texture2Size).rgb * mask.g;
	vec3 tex3 = texture(texture3, fTexCoord * texture3Size).rgb * mask.b;
	vec3 tex4 = texture(texture4, fTexCoord * texture4Size).rgb * mask.a;
	
	outPosition = fPosition;
	outNormal = fNormal;
    outDiffuse = vec4(tex1 + tex2 + tex3 + tex4, 0);
	outSpecular = vec4(texture(specular, fTexCoord).rgb, 0.01);
}
