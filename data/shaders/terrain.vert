// terrain.vert
#version 330 core

uniform mat4 projection;
uniform mat4 view;
uniform vec3 scale;
uniform sampler2D heightMap;
uniform float patchSize;

in vec2 vPosition;
in vec2 vTexCoord;

out vec3 fPosition;
out vec3 fNormal;
out vec2 fTexCoord;

void main()
{
    float height = texture(heightMap, vTexCoord).x; // Height of this vertex
    float lHeight = texture(heightMap, vTexCoord + vec2(-patchSize, 0)).x; // Height of vertex to left
    float rHeight = texture(heightMap, vTexCoord + vec2(patchSize, 0)).x; // Height of vertex to right
    float tHeight = texture(heightMap, vTexCoord + vec2(0, patchSize)).x; // Height of vertex above
    float bHeight = texture(heightMap, vTexCoord + vec2(0, -patchSize)).x; // Height of vertex below
    
    vec3 rightLeft = normalize(vec3(2 * patchSize, rHeight - lHeight, 0) * scale); // Vector between right and left vertices
    vec3 topBottom = normalize(vec3(0, tHeight - bHeight, 2 * patchSize) * scale); // Vector between above and below vertices

    vec3 position = vec3(vPosition.x, height, vPosition.y) * scale; // Position of this vertex
    vec3 normal = cross(topBottom, rightLeft); // Normal of this vertex
    
    fTexCoord = vTexCoord;
    fPosition = position;
    fNormal = normal;
    gl_Position = projection * view * vec4(position, 1);
}
