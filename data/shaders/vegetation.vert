// vegetation.vert
#version 330 core

uniform mat4 view;
uniform mat4 projection;
uniform float time;

in vec3 vPosition;
in vec3 vNormal;
in vec2 vCoord;
in mat4 model;

out vec3 fPosition;
out vec3 fNormal;
out vec2 fCoord;

void main()
{
    // Compute this vertex's sway factor
    float swayFactor = length(vPosition);

    // Sway the vertex
    vec3 position = vec3(vPosition.x, sin(time + vPosition.x) / 30 * swayFactor + vPosition.y, vPosition.z);

	gl_Position = projection * view * model * vec4(position, 1);
	fPosition = (model * vec4(position, 1)).xyz;
	fNormal = (transpose(inverse(model)) * vec4(vNormal, 0)).xyz;
	fCoord = vCoord;
}
