// skybox.frag
#version 330 core

uniform sampler2D diffuse;

in vec3 fPosition;
in vec3 fNormal;
in vec2 fCoord;

layout (location = 0) out vec3 outPosition;
layout (location = 1) out vec3 outNormal;
layout (location = 2) out vec4 outDiffuse;
layout (location = 3) out vec4 outSpecular;

void main()
{
    outPosition = fPosition;
    outNormal = normalize(fNormal);
    outDiffuse = vec4(texture(diffuse, fCoord).rgb, 0.3);
	outSpecular = vec4(0, 0, 0, 0.1); // No specular
}
