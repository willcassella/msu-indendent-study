// fern.frag
#version 330 core

uniform sampler2D diffuse;
uniform sampler2D normal;

in vec3 fPosition;
in vec3 fNormal;
in vec2 fCoord;

layout (location = 0) out vec3 outPosition;
layout (location = 1) out vec3 outNormal;
layout (location = 2) out vec4 outDiffuse;
layout (location = 3) out vec4 outSpecular;

void main()
{
    vec4 diffColor = texture(diffuse, fCoord);

    if (diffColor.a < 0.5)
    {
        discard;
    }

    outPosition = fPosition;
    outNormal = normalize(fNormal);// + texture(normal, fCoord).xyz);
    outDiffuse = vec4(diffColor.rgb, 0);
	outSpecular = vec4(1, 1, 1, 1); // White and very shiny
}
