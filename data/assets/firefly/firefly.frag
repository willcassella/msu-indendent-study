// firefly.frag
#version 330 core

in vec3 fPosition;
in vec3 fNormal;
in vec2 fCoord;

layout (location = 0) out vec3 outPosition;
layout (location = 1) out vec3 outNormal;
layout (location = 2) out vec4 outDiffuse;
layout (location = 3) out vec4 outSpecular;

void main()
{
    outPosition = fPosition;
    outNormal = normalize(fNormal);
    outDiffuse = vec4(1, 0.929, 0.388, 1);
	outSpecular = vec4(1, 1, 1, 0.1); // White and not very shiny
}
