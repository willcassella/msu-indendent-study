// EventManager.h - Copyright 2013-2015 Will Cassella, All Rights Reserved
#pragma once

#include <vector>
#include <memory>
#include <unordered_map>
#include "EventHandler.h"
using std::vector;
using std::unique_ptr;
using std::unordered_map;

struct EventManager final
{
    ///////////////////
    ///   Methods   ///
public:

    /** Dispatch an event */
    void DispatchEvent(const Event& event);

    /** Binds the given event handler on the given object to the given event */
    template <class OwnerType, typename ArgType>
    void Bind(const string& eventName, OwnerType* object, void(OwnerType::*handler)(ArgType))
    {
        static_assert(!std::is_reference<ArgType>::value || std::is_const<typename std::remove_reference<ArgType>::type>::value,
                      "The argument type of an event handler must either be a value or a const reference.");

        _handlers[eventName].push_back(std::make_unique<TEventHandler<OwnerType, ArgType>>(object, handler));
    }

    /** Binds the given event handler on the given object to the given event */
    template <class OwnerType>
    void Bind(const string& eventName, OwnerType* object, void(OwnerType::*handler)())
    {
        _handlers[eventName].push_back(std::make_unique<TEventHandler<OwnerType, void>>(object, handler));
    }

    ////////////////
    ///   Data   ///
private:

    unordered_map<string, vector<unique_ptr<IEventHandler>>> _handlers;
};
