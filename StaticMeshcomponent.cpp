// StaticMeshComponent.cpp - Copyright 2013-2015 Will Cassella, All Rights Reserved

#include "StaticMeshComponent.h"

////////////////////////
///   Constructors   ///

StaticMeshComponent::StaticMeshComponent(GameObject& owner)
    : Component(owner)
{
    // All done
}
