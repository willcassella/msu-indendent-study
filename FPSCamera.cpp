// FPSCamera.cpp - Copyright 2013-2015 Will Cassella, All Rights Reserved

#include "Scene.h"
#include "FPSCamera.h"

////////////////////////
///   Constructors   ///

FPSCamera::FPSCamera(Scene& scene)
    : Actor(scene), Camera(This)
{
    Camera.ZFar = 500;

    scene.eventManager.Bind("Move", this, &FPSCamera::Move);
    scene.eventManager.Bind("Look", this, &FPSCamera::Look);
    scene.eventManager.Bind("Boost", this, &FPSCamera::Boost);
    scene.eventManager.Bind("Resize", this, &FPSCamera::Resize);
}

//////////////////////////
///   Event Handlers   ///

void FPSCamera::Tick(float timeDelta)
{
    _speed = timeDelta;
    SnapToTerrain();
    transform.Position.Y += 1;
}

void FPSCamera::Move(const Vec3& dir)
{
    float currentY = transform.Position.Y;
    transform.Translate(dir.Normalize() * _speed, true);
    transform.Position.Y = currentY;
}

void FPSCamera::Look(const Vec2& mouse)
{
    transform.Rotate(Vec3(0, 1, 0), mouse.X/100, false);
    transform.Rotate(Vec3(1, 0, 0), mouse.Y/100, true);
}

void FPSCamera::Boost()
{
    _speed *= 2;
}

void FPSCamera::Resize(const Vec2& screen)
{
    // Compute the correct vertical field of view to maintain the desired horizontal field of view without distortion
    float RadHFOV = Camera.HFOV * Deg2Rad;
    Camera.VFOV = Rad2Deg * 2 * atan(tan(RadHFOV * 0.5f) * 1/(screen.X / screen.Y));
}
