// main.cpp

#include <QApplication>
#include "GLWidget.h"

int main(int argc, char** argv)
{
    QApplication a(argc, argv);

    // Setup OpenGL properties
    QSurfaceFormat format;
    format.setVersion(3,3);
    format.setProfile(QSurfaceFormat::CoreProfile);
    QSurfaceFormat::setDefaultFormat(format);

    GLWidget widget;

    // Run Application
    widget.show();
    return a.exec();
}
