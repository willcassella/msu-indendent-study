// FireFLy.h
#pragma once

#include "GameObject.h"
#include "LightComponent.h"
#include "StaticMeshComponent.h"

class Firefly : public GameObject
{
    ////////////////////////
    ///   Constructors   ///
public:

    Firefly(Scene& scene);

    //////////////////////
    ///   Components   ///
public:

    StaticMeshComponent Mesh;
    LightComponent Light;

    //////////////////////////
    ///   Event Handlers   ///
public:

    void Tick();
};
