// Component.h - Copyright 2013-2015 Will Cassella, All Rights Reserved
#pragma once

////////////////////////////////
///   Forward-declarations   ///

/** Defined in "GameObject.h */
class GameObject;

/////////////////
///   Types   ///

class Component
{
    ////////////////////////
    ///   Constructors   ///
public:

    Component(GameObject& owner);
    Component(const Component& copy) = delete;
    Component(Component&& move) = delete;
    virtual ~Component();

    ///////////////////
    ///   Methods   ///
public:

    /** Returns a reference to the GameObject that owns this Component */
    GameObject& GetOwner();

    /** Returns an immutable reference to the GameObject that owns this Component */
    const GameObject& GetOwner() const;

    /** Returns whether this Component is currently enabled */
    bool IsEnabled() const;

    /** Enables this Component if it is currently disabled */
    void Enable();

    /** Customizable behavior for when this Component is enabled */
    virtual void OnEnable();

    /** Disables this Component if it is currently enabled */
    void Disable();

    /** Customizable behavior for when this Component is disabled */
    virtual void OnDisable();

    /////////////////////
    ///   Operators   ///
public:

    Component& operator=(const Component& copy);
    Component& operator=(Component&& move);

    ////////////////
    ///   Data   ///
private:

    GameObject* _owner;
    bool _isEnabled = true;
    bool _isInitialized = false;
};
